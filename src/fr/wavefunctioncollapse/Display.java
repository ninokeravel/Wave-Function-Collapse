package fr.wavefunctioncollapse;

import java.awt.Canvas;
import java.awt.Dimension;


import javax.swing.JFrame;

public class Display {
    private JFrame jframe;
    private static Canvas canvas;
    private String title;
    private int size;

    public Display(String title, int size) {
        this.title = title;
        this.size = size;
        initCanvas();
    }

    private void initCanvas() {

        jframe = new JFrame(title);
        jframe.setSize(size, size);
        jframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jframe.setResizable(false);
        jframe.setVisible(true);
        jframe.setLocationRelativeTo(null);

        canvas = new Canvas();
        canvas.setPreferredSize(new Dimension(size, size));
        canvas.setMaximumSize(new Dimension(size, size));
        canvas.setMinimumSize(new Dimension(size, size));

        jframe.add(canvas);
        jframe.pack();

    }

    public Canvas getCanvas() {

        if(canvas == null)
        {
            System.out.println("Canvas is null");
            return null;
        }

        return canvas;
    }
}