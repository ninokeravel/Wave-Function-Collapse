package fr.wavefunctioncollapse;

import java.util.List;
import java.util.ArrayList;

public class WaveFunction implements Comparable<WaveFunction>{
	List<Tile> possibilities;
	WaveFunction[] neighbors;
	public int x,y;
	public WaveFunction(Tile[] collection,int x, int y) {
		possibilities = new ArrayList<Tile>();
		for (int i=0;i<collection.length;i++) {
			possibilities.add(collection[i]);
		}
		this.x = x;
		this.y = y;
	}
	
	public static WaveFunction[][] initGrid(Tile[] collection,int size){
		WaveFunction[][] grid = new WaveFunction[size][size];
		for (int i=0;i<size;i++) {
			for (int j=0;j<size;j++) {
				grid[i][j] = new WaveFunction(collection,i,j);
			}
		}
		
		for (int i=0;i<size;i++) {
			for (int j=0;j<size;j++) {
				WaveFunction[] ns = new WaveFunction[4];
				if (i>0) {
					ns[3] = grid[i-1][j];
				}
				if (i<size-1) {
					ns[1] = grid[i+1][j];
				}
				if (j<size-1) {
					ns[2] = grid[i][j+1];
				}
				if (j>0) {
					ns[0] = grid[i][j-1];
				}
				grid[i][j].setNeighbors(ns);
			}
		}
		
		
		return grid;
	}
	
	public void setNeighbors( WaveFunction[] neighbors) {
		this.neighbors = neighbors;
	}
	
	public boolean check() {
		ArrayList<Tile> newPossibilities = new ArrayList<Tile>();
		int lp = possibilities.size();
		while(!possibilities.isEmpty()) {
			Tile current = possibilities.remove(0);
			boolean possible = true;
			for (int n=0;n<4;n++) {
				if (neighbors[n] != null){
					boolean tempPossibility = false;
					for (int p=0;p<neighbors[n].possibilities.size();p++) {
						tempPossibility |= current.map.get(neighbors[n].possibilities.get(p))[n];
					}
					possible &= tempPossibility;
					
				}
			}
			if (possible) {
				newPossibilities.add(current);
			}
		}
		possibilities.addAll(newPossibilities);
		
		System.out.println("Removed "+(lp-possibilities.size()));
		return (lp-possibilities.size())>0;
	}
	
	
	public Tile collapse() {
		if (possibilities.size() == 1){
			return possibilities.get(0);
		}
		else
		{
			int i = (int)(Math.random()*possibilities.size()); // create a random number between 0 and the length
			Tile r = possibilities.get(i);
			possibilities.clear();
			possibilities.add(r);
			return r;
		}
	}

	@Override
	public int compareTo(WaveFunction wf) {
		
		return this.possibilities.size()-wf.possibilities.size();
	}
	
}
