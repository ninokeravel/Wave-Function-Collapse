package fr.wavefunctioncollapse;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Grid {
	
	public Tile[][] tileGrid;
	public int gridsize;
	public WaveFunction[][] waveGrid;
	public Tile[] collection;
	
	public Grid(int gridsize,Tile[] collection) {
		this.gridsize = gridsize;
		this.collection = collection;
		tileGrid = new Tile[gridsize][gridsize];
		waveGrid = WaveFunction.initGrid(collection, gridsize);
	}
	
	public boolean collapse() {
		List<WaveFunction> toCollapse = new ArrayList<WaveFunction>();
		List<WaveFunction> checked = new ArrayList<WaveFunction>();
		List<WaveFunction> toCheck = new ArrayList<WaveFunction>();
		List<WaveFunction> notCollapsed = new ArrayList<WaveFunction>();
		List<WaveFunction> Collapsed = new ArrayList<WaveFunction>();
		for (int i=0;i<gridsize;i++) {
			for (int j=0;j<gridsize;j++) {
				if(tileGrid[i][j] == null) {
					if (waveGrid[i][j].possibilities.size()==1) {
						toCollapse.add(waveGrid[i][j]);
						checked.add(waveGrid[i][j]);
						
					} else {
						notCollapsed.add(waveGrid[i][j]);
					}
				} else {
					Collapsed.add(waveGrid[i][j]);
				}				
			}
		}
		if (toCollapse.size() == 0) {
			if (notCollapsed.size()<1) {
				return false;
			}
			Collections.sort(notCollapsed);
			WaveFunction wf = notCollapsed.get(0);
			toCollapse.add(wf);
		}
		
		for (int i=0;i<toCollapse.size();i++) {
			System.out.println("collapsing : "+toCollapse.get(i).x+ " "+toCollapse.get(i).y);
			for (int ns = 0; ns<4;ns++) {
				if (toCollapse.get(i).neighbors[ns] != null && notCollapsed.contains(toCollapse.get(i).neighbors[ns])) {
					toCheck.add(toCollapse.get(i).neighbors[ns]);
				}
			}
			tileGrid[toCollapse.get(i).x][toCollapse.get(i).y] = toCollapse.get(i).collapse();
		}
		while (!toCheck.isEmpty()) {
			WaveFunction current = toCheck.remove(0);
			boolean removed = current.check();
			checked.add(current);
			
			if (removed){
				for (int n=0;n<4;n++) {
					if (current.neighbors[n]!=null && !(Collapsed.contains(current.neighbors[n]) || checked.contains(current.neighbors[n]) || toCheck.contains(current.neighbors[n]))) {
						toCheck.add(current.neighbors[n]);
					}
				}
			}
		}
		
		for (int i=0;i<gridsize;i++) {
			for (int j=0;j<gridsize;j++){
				if (tileGrid[i][j]==null) {
					return true;
				}
			}
		}
		return false;
		
	}
}
