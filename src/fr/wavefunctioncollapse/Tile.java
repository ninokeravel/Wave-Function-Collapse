package fr.wavefunctioncollapse;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.HashMap;
import java.util.Map;



public class Tile {
	public BufferedImage image;
	public String name;

	Map<Tile,boolean[]> map = new HashMap<Tile,boolean[]>();
	
	
	public Tile(String path,int rot, int tilesize) {
		image=ImageLoader.loadImage(path);
		BufferedImage resized = new BufferedImage(tilesize,tilesize,image.getType());
	    Graphics2D graphics2D = resized.createGraphics();
	    graphics2D.drawImage(image, 0, 0, tilesize, tilesize, null);
	    graphics2D.dispose();
	    image = resized.getSubimage(0, 0, tilesize, tilesize);
		for (int r=0;r<rot;r++)
		{
		    int width  = image.getWidth();
		    int height = image.getHeight();
		    BufferedImage newImage = new BufferedImage( tilesize, tilesize, image.getType() );

		    for( int i=0 ; i < width ; i++ )
		        for( int j=0 ; j < height ; j++ )
		            newImage.setRGB( height-1-j, i, image.getRGB(i,j) );

		    image = newImage;
		}
		name = path+rot;
	}
	
	public static Tile[] load(String folder, int tilesize) {
		String floc = System.getProperty("user.dir")+folder;
		File f = new File(floc);
		File[] listOfFile = f.listFiles();
		int size = listOfFile.length;
		Tile[] collection = new Tile[size*4];
		for (int i=0;i<size;i++) {
			for (int rot=0;rot<4;rot++) {
				collection[i*4+rot] = new Tile(folder+listOfFile[i].getName(),rot,tilesize);
			}
			
		}
		
		for (int i=0;i<size*4;i++) {
			for (int j=0;j<size*4;j++) {
				//System.out.println(listOfFile[i].getName()+" "+listOfFile[j].getName()+" : ");
				collection[i].map.put(collection[j], generateMap(collection[i],collection[j]));
				//System.out.println(i+" "+j+" : "+collection[i].map.get(collection[j])[0][0]);
			}
		}
		
		return collection;
	}

	public static boolean[] generateMap(Tile t1, Tile t2){
		// generate all possible connections between two adjacent tile t1 and t2 dependanding on their relative position
		// order of relative postition of t2 for t1: TOP RIGHT BOTTOM LEFT
		int sample = 3;
		int tilesize = t1.image.getHeight()-1;
		boolean[] map = new boolean[4];
		
		if (sample==1) {
		// TOP
		map[0] = t1.image.getRGB(tilesize/2, 0) == t2.image.getRGB(tilesize/2, tilesize);
		
		//RIGHT
		map[1] = t1.image.getRGB(tilesize, tilesize/2) == t2.image.getRGB(0, tilesize/2);
		
		//BOTTOM
		map[2] = t1.image.getRGB(tilesize/2, tilesize) == t2.image.getRGB(tilesize/2, 0); 

		//RIGHT
		map[3] = t1.image.getRGB(0, tilesize/2) == t2.image.getRGB(tilesize, tilesize/2); 
		} else {
			BufferedImage img1 = t1.image;
			BufferedImage img2 = t2.image;
			//TOP
			boolean top = true;
			for (int s=0;s<tilesize;s+=tilesize/(sample-1)) {
				top &= img1.getRGB(s, 0) == img2.getRGB(s, tilesize);
			}
			map[0] = top;
			//RIGHT
			boolean right = true;
			for (int s=0;s<tilesize;s+=tilesize/(sample-1)) {
				right &= img1.getRGB(tilesize, s) == img2.getRGB(0, s);
			}
			map[1] = right;
			//BOTTOM
			boolean bottom = true;
			for (int s=0;s<tilesize;s+=tilesize/(sample-1)) {
				bottom &= img1.getRGB(s, tilesize) == img2.getRGB(s, 0);
			}
			map[2] = bottom;
			//RIGHT
			boolean left = true;
			for (int s=0;s<tilesize;s+=tilesize/(sample-1)) {
				left &= img1.getRGB(0, s) == img2.getRGB(tilesize, s);
			}
			map[3] = left;
		}
		
		
		
		
		return map;
	}

	@SuppressWarnings("unused")
	private static String color(int rgb) {
		int r = (rgb>>16)&0xFF;
		int g = (rgb>>8)&0xFF;
		int b = (rgb>>0)&0xFF;
		return "("+r+","+g+","+b+")";
	}
}
