package fr.wavefunctioncollapse;

import java.awt.Graphics;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;



public class DrawImageOnCanvas{
    private Display Display;
    public int size;
    public int tilesize;
    public int gridsize;
    private String title;
    private BufferStrategy bs;
    private Graphics g;

    public DrawImageOnCanvas(String title, int tilesize,int gridsize) {
        this.title = title;
        this.size = tilesize*gridsize;
        this.tilesize = tilesize;
        this.gridsize = gridsize;

    }

    

    public void render(Tile[][] tileGrid) {
        bs = Display.getCanvas().getBufferStrategy();

        if (bs == null) {
            System.out.println("bs is null....");
            Display.getCanvas().createBufferStrategy(3);
            //return;
        }
        g = Display.getCanvas().getGraphics();
        for (int i=0;i<gridsize;i++) { 
        	for (int j=0;j<gridsize;j++) {
        		if (tileGrid[i][j]!=null) {
        			BufferedImage image = tileGrid[i][j].image;
        			g.drawImage(image, i*tilesize, j*tilesize, null);
        		}
        	}
        }
    }





    public void init() {
        Display = new Display(title, size);
    }



}