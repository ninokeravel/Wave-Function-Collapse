package fr.wavefunctioncollapse;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public final class ImageLoader
{

    static BufferedImage loadImage(String fileName)
    {
        BufferedImage bi = null;
        File f = new File(System.getProperty("user.dir")+fileName);
        //System.err.println("....setimg...." + fileName + f.exists());

        try {
            bi = ImageIO.read(f); 

        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Image could not be read");
            System.exit(1);
        }

        return bi;
    }
}