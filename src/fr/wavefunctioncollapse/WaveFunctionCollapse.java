package fr.wavefunctioncollapse;



public class WaveFunctionCollapse {
	
	
	static int gridsize = 64;
	static int tilesize = 16;
	static int size = gridsize*tilesize;
	static String folder = "/src/tiles/circuit/";
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		DrawImageOnCanvas drawer = new DrawImageOnCanvas("image",tilesize,gridsize);
		drawer.init();

		Tile[] collection = Tile.load(folder,tilesize);
		Grid grid = new Grid(gridsize,collection);
		
		grid.waveGrid[gridsize/2][gridsize/2].collapse();
		
		while (grid.collapse()) {
			
			drawer.render(grid.tileGrid);
		}
		drawer.render(grid.tileGrid);
	}

}
